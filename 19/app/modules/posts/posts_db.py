class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username):
        self.conn.insert({'post':post, 'username': username})

    def getItems(self, username):
    	return self.conn.find({'username': username})

    def createItem(self, item, price, username):
    	self.conn.insert({'item':item, 'price': price, 'username': username})
