from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def item_list():
    items = g.postsdb.getItems(session['username'])
    cart = g.cartdb.getCart(session['username'])
    return render_template('posts/post.html', items=items, cart=cart, date='November 26, 2015')

@mod.route('/', methods=['POST'])
def create_item():
    #new_post = request.form['new_post'] + ' ' + request.form['emo']
    if request.form['submit'] == 'Submit':
        if request.form['item'] != "" and request.form['price'] != "":
	    	new_item = request.form['item']
	    	new_price = int(request.form['price'])
	    	g.postsdb.createItem(new_item, new_price, session['username'])
	    	#g.cartdb.createCart(new_item, new_price, 6, session['username'])
		return redirect(url_for('.item_list'))
    elif request.form['submit'] == 'Add To Cart':
        items = g.postsdb.getItems(session['username'])
        for item in items:
        	i = item['item']
        	if request.form[i] != "":
        		q = int(request.form[i])
        		price_total = q * int(item['price'])
        		g.cartdb.createCart(i, price_total, q, session['username'])
        return redirect(url_for('.item_list'))
    elif request.form['submit'] == 'Purchase':
    	items = g.postsdb.getItems(session['username'])
    	cart = ""
       	return render_template('posts/post.html', items=items, cart=cart, date='November 26, 2015')
    else:
        return redirect(url_for('.item_list'))
